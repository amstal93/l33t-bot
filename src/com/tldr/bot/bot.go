package main

import (
	"com/tldr/bot/impl"
	"fmt"
	"io/ioutil"
	"log"
	"sync"
)

func main() {
	bytes, err := ioutil.ReadFile("bot-token.txt")
	if err != nil {
		log.Fatal("Error opening bot token file! " + err.Error())
		return
	}

	impl.BotToken = string(bytes)
	fmt.Println("Starting to work with bot token: " + impl.BotToken)

	join := sync.WaitGroup{}
	join.Add(1)

	poller := impl.Poller{}
	poller.Start(&join)

	join.Wait()
}
